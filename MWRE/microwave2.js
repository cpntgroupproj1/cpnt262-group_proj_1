     
var timeInput = "";
var seconds = 60;

function addTime(buttonElement){

   timeInput+=document.getElementById(buttonElement.id).value;

   
   document.getElementById('countdown').innerHTML=timeInput;

}
 
function startTimer() 
{
    var minutes = Math.round((seconds - 30)/60);
    var remainingSeconds = seconds % 60;       
    
    document.getElementById("countdown").innerHTML = minutes + ":" + remainingSeconds;
    if (seconds == 0) 
    {	
    	var audio = document.getElementsByTagName('audio')[0];

    	clearInterval(countdownTimer);

        document.getElementById("countdown").innerHTML = "Ready!"
        audio.play();
    } 
    else
    {
        seconds--;
    }
}
  
var countdownTimer = setInterval('startTimer()', 1000);